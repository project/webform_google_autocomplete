CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Webform Google Autocomplete adds autocomplete functionality to webforms.


REQUIREMENTS
------------

 * Webform Module


INSTALLATION
------------

1. Install as usual, see https://www.drupal.org/node/895232 for further
   information.


CONFIGURATION
-------------

Navigate to Admin -> Configuration -> Web Services -> Webform Google Autocomplete Settings.  Enter your Google API key.

Create a webform and add components. For textfield components, check Use
autocompletion and select the option, and it will add the autocompletion to the
textfield.


MAINTAINERS
-----------

Current maintainers:
* Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
