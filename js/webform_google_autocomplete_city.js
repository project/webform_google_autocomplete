(function ($) {

  /**
   * Script for autocomplete address field.
   */
  Drupal.behaviors.webformGoogleAutocompleteCity = {
    attach: function (context, settings) {
      $('.webform-google-autocomplete-city').focus(function() {
        var autocomplete;
        var $element = $('.webform-google-autocomplete-city');

        // Preventing Chrome from adding auto complete.
        var observerHack = new MutationObserver(function() {
          observerHack.disconnect();
          $element.attr("autocomplete", "new-password");
        });
        observerHack.observe($element[0], {
          attributes: true,
          attributeFilter: ['autocomplete']
        });

        initAutocomplete($element);
      });

      var initAutocomplete = function (element) {
        var options = {
          types: ['(cities)'],
          element: element
        };
        autocomplete = new google.maps.places.Autocomplete(element[0], options);

        // When the user selects an address from the dropdown, populate the
        // city only in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }
      var fillInAddress = function() {
        // Just in case there is no result, setting it to blank.
        autocomplete.element.val('');

        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        if (typeof place !== "undefined") {
          for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (addressType === 'locality') {
              var val = place.address_components[i]['long_name'];
              autocomplete.element.val(val);
            }
          }
        }
      }
    }
  };

})(jQuery);
