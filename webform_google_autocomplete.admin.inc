<?php

/**
 * @file
 * Admin functionality.
 */

/**
 * Webform Google autocomplete settings form.
 */
function webform_google_autocomplete_settings_form($form, &$form_state) {
  $form['webform_google_autocomplete_google_api_key'] = [
    '#type' => 'textfield',
    '#title' => t('Google API Key'),
    '#default_value' => variable_get('webform_google_autocomplete_google_api_key'),
    '#size' => 50,
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}
